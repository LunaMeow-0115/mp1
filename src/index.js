/*
 * This is the main entry point for Webpack, the compiler & dependency loader.
 * All files that are necessary for your web page and need to be 'watched' for changes should be included here!
 */

// Import files.
// HTML Files
import './index.html';
// Stylesheets
import './css/main.scss';
// Scripts
import './js/main.js';


// Navigation bar.
// Requirement 4: Navbar will shrink if users scroll down the webpage.
// After DOMContent loaded, the function will be initiated.
document.addEventListener("DOMContentLoaded", function () {
    // Get element to control.
    const navbar = document.getElementById("navbar");
    // When the window is being scrolled, the event will happen.
    window.addEventListener("scroll", function () {
        // If scroll distance exceeds 50px, shrink will happen, else, not.
        if (window.scrollY> 50) {
            navbar.classList.add('shrink');
        } else {
            navbar.classList.remove('shrink');
        }
    });
});

// Requirement 5: When a section's menu is clicked, the webpage will locate to that part.
document.querySelectorAll('.nav-item').forEach(item => {
    // For every nav-item, when it is clicked, get the corresponding data target and element.
    item.addEventListener('click', function() {
        const targetSectionId = this.getAttribute('data-target');
        const targetSection = document.getElementById(targetSectionId);
        // Smoothly scroll to that part.
        targetSection.scrollIntoView({ behavior: 'smooth' });
    });
});



// Requirement 3: The menu will be highlighted when the webpage is on the corresponding section.
document.addEventListener("scroll", function() {
    // Get all sections.
    let sections = document.querySelectorAll("section");
    // Get the height of navbar.
    let navbarHeight = document.querySelector("#navbar").getBoundingClientRect().height;
    // Set it as the current active section.
    let activeSection = null;

    sections.forEach(section => {
        // Get every section's position.
        let secposi = section.getBoundingClientRect();
        // If the section is just under navbar.
        if (secposi.top <= navbarHeight && secposi.bottom >= navbarHeight) {
            activeSection = section;
        }
    });

    // If users scroll down to the bottom of webpage, like the footer, set up the last section as active.
    if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
        activeSection = sections[sections.length - 1];
    }

    // If there is an active section,
    if (activeSection) {
        // choose the corresponding item,
        let activeItem = document.querySelector(`#navbar .nav-item[data-target="${activeSection.id}"]`);
        // loop through all menus, delete active for the one which is not active, and make the right one active.
        let items = document.querySelectorAll("#navbar .nav-item");
        items.forEach(item => item.classList.remove("active")); // 移除所有高亮
        activeItem.classList.add("active"); // 为当前活动的菜单项添加高亮
    }
});



// Section 1: Carousel.
// Requirement 6: make a carousel, containing slide buttons.
// Requirement 13: make fade in animation.
// If the left button is clicked, the slide scroll to the left by one window's width.
document.querySelector(".left-slide-btn").addEventListener("click", function() {
    document.querySelector(".slides").scrollBy({
        left: -window.innerWidth,
        behavior: 'smooth'
    });
});

// Right slide button.
document.querySelector(".right-slide-btn").addEventListener("click", function() {
    document.querySelector(".slides").scrollBy({
        // Moves one screen width to the right
        left: window.innerWidth,
        behavior: 'smooth'
    });
});

// Text animation fade in happens every time when the button is clicked.
function handleSlideButtonClick(direction) {
    const slideContents = document.querySelectorAll(".slide-content");
    slideContents.forEach(slide => {
        slide.classList.remove('fade-in-animation');
        void slide.offsetWidth;  // 强制重排
        slide.classList.add('fade-in-animation');    // 重新添加动画类
    });

    const scrollAmount = (direction === "left" ? -window.innerWidth : window.innerWidth);
    document.querySelector(".slides").scrollBy({
        left: scrollAmount,
        behavior: 'smooth'
    });
}

// Realize the previous effect by clicking button.
document.querySelector(".left-slide-btn").addEventListener("click", function() {
    handleSlideButtonClick("left");
});

document.querySelector(".right-slide-btn").addEventListener("click", function() {
    handleSlideButtonClick("right");
});



// Section 3: Requirement 11; Modal.
// Open a modal.
function openModal(modalId) {
    const modal = document.getElementById(modalId);
    if (modal) {
        modal.style.display = 'block';
    }
}

// Requirement 14: svg.
document.body.addEventListener("click", function(event) {
    // click happens to icon.
    if (event.target.closest('.icon')) {
        const iconElem = event.target.closest('.icon');
        // Realize open modal.
        const modalId = iconElem.getAttribute('data-modal-id');
        if (modalId) {
            openModal(modalId);
        }
    }
});


// Close a modal.
function closeModal(modalId) {
    const modal = document.getElementById(modalId);
    if (modal) {
        modal.style.display = 'none';
    }
}

// When click close button, close window.
document.body.addEventListener("click", function(event) {
    if (event.target.matches(".close-btn")) {
        const modal = event.target.closest(".modal");
        if (modal && modal.id) {
            closeModal(modal.id);
        }
    }
});

// Close the modal if clicked outside the content.
// Modal is not modal-content, so it means if users click other parts outside a modal, the modal content will disappear.
window.onclick = function(event) {
    let modalElements = document.getElementsByClassName("modal");
    Array.from(modalElements).forEach((modal) => {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    });
}

